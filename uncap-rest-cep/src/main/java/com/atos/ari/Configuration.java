package com.atos.ari;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;


public class Configuration {
	
	private static Configuration instance = null;
	public Properties config;
	
	public static Configuration getInstance() throws Exception{
		if (instance == null){
			instance = new Configuration();
		}
		return instance;
	}
	
	private Configuration() throws Exception{
		config = new Properties();
		config.load(new FileInputStream("./resources/config.properties"));
		PropertyConfigurator.configure("./resources/log4j.properties");
	}
}
