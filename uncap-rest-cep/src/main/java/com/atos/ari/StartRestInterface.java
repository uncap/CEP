package com.atos.ari;

import java.io.IOException;
import java.net.URI;

import org.apache.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Main class.
 *
 */
public class StartRestInterface {
	public final static String DOLCE_PATH_KEY="dolce_file_path";
	public final static String USER_CONF_PATH_KEY="users_configurations";	
	final static Logger logger = Logger.getLogger(StartRestInterface.class);	
	
    // Base URI the Grizzly HTTP server will listen on
    public static String BASE_URI = null;

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.atos.ari package
        final ResourceConfig rc = new ResourceConfig().packages("com.atos.ari");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws Exception {
    	loadConfiguration();
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl", BASE_URI));
        //System.in.read();
        
    }
    
	private static void loadConfiguration() throws Exception {
		BASE_URI = Configuration.getInstance().config.getProperty("url");
	}
    
}

