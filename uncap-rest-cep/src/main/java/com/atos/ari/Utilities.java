package com.atos.ari;

import java.io.FileReader;
import java.util.List;

import org.apache.log4j.Logger;

import com.atos.ari.mongo.MongoHelper;

public class Utilities {
	
	final static Logger logger = Logger.getLogger(Utilities.class);	
	
	public  String getDevices(String user_id){
		try{
			MongoHelper mh = MongoHelper.getInstance();
			return mh.getDevices(user_id).toJson();
		}catch (Exception e){
			logger.error(e);
			return null;
		}
	}
	
	String formatStringJson(String cad){
		return "\""+cad+"\"";
	}
	
	public String getRules(String user_id, String device_id) {
		try{
			MongoHelper mh = MongoHelper.getInstance();
			return mh.getRules(user_id, device_id).toJson();
		}catch (Exception e){
			logger.error(e);
			return null;
		}
	}

	public String setRules(String user_id, String device_id, String rule_id,String param_name, String param_value) {
		String res = null;
		try{
			MongoHelper mh = MongoHelper.getInstance();
			res = mh.setRules(user_id, device_id, rule_id, param_name, param_value);
			return res; 
		}catch (Exception e){
			logger.error(e);
			return "ERROR";
		}
	}
	
}
