package com.atos.ari.mongo;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.Document;

import com.atos.ari.Configuration;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;

public class MongoHelper {
	final static Logger logger = Logger.getLogger(MongoHelper.class);	
	private static  MongoHelper instance=null;
	private String host;
	private int port;
	private String db;
	private MongoDatabase mongoDB;
	private MongoHelper()  throws Exception{
		super();
    	this.host = Configuration.getInstance().config.getProperty("mongo.host");
    	this.port = Integer.parseInt(Configuration.getInstance().config.getProperty("mongo.port"));
    	this.db = Configuration.getInstance().config.getProperty("mongo.dbname");
		MongoClient mongoClient = new MongoClient( this.host, this.port );
		mongoDB = mongoClient.getDatabase(db);
	}
	
	public static  MongoHelper  getInstance() throws Exception{
		if (instance == null){
			instance = new MongoHelper();
		}
		return instance;
	}
	
	
	public Document getDevices(String user_id){
		Document res = null;
		MongoCollection<Document> coll =mongoDB.getCollection("configurations");
		Document docQuery = new Document("user_id",user_id );
		FindIterable<Document> iterable = coll.find(docQuery);
		res = new Document();
		List<String> items = new ArrayList<String>();
		//if no configurations we use "default" user
		if (items.size()<=0) {
			 docQuery = new Document("user_id","default" );
			 iterable = coll.find(docQuery);
			items = new ArrayList<String>();
		}
		
		MongoCursor<Document> mc = iterable.iterator();
		while (mc.hasNext()){
			Document doc = mc.next();
			String deviceId = doc.get("device_id").toString();
			items.add(deviceId);
		}
		//String sjson = "{\"devices\":[\"hola\",\"adios\"]}";
		//res = Document.parse(sjson);
		res = new Document("devices", items);
		return res;
	}
	
	public Document getRules(String user_id, String device_id){
		Document res = null;
		MongoCollection<Document> coll  =mongoDB.getCollection("configurations");
		Document docQuery = new Document("user_id",user_id ).append("device_id", device_id);
		Document docDefaultQuery = new Document("user_id","default" ).append("device_id", device_id);		
		FindIterable<Document> iterable = coll.find(docQuery);
		res = iterable.first();
		/*
		if (res==null){
			logger.debug("No configuration for user_id " + user_id + " default configuration is returned");
			iterable = coll.find(docDefaultQuery);
			res = iterable.first();
		}
		*/
		return res;
	}
	
	
	public String setRules(String user_id, String device_id, String rule_id, String param_name, String param_value){
		String res = null;
		MongoCollection< Document>  coll = mongoDB.getCollection("configurations");
		Document docUser = new Document("user_id",user_id).append("device_id",device_id);
		FindIterable<Document> iterator =  coll.find(docUser);
		Document docRules= iterator.first();
		if (docRules==null){
			res = insertNewUser(coll, user_id, device_id, rule_id,param_name,param_value);
		}else{
			res = update(coll, docUser,  docRules, rule_id, param_name, param_value);
		}
		return res;
		
	}

	private String insertNewUser(MongoCollection< Document>  coll, String user_id, String device_id,
			String rule_id, String param_name, String param_value) {
		Document docUser = new Document("user_id",user_id).append("device_id",device_id);	
		List<Document> rules = new ArrayList<Document>(); 
		docUser.append("rules", rules);
		List<Document> items = new ArrayList<Document>();
		items.add(new Document("field_name",param_name).append("field_value",param_value));
		rules.add(new Document(rule_id, items ));
		coll.insertOne(docUser);
		return "OK";		
	}

	private String update(MongoCollection<Document> coll, Document docUser, Document docRules, String rule_id, String param_name, String param_value) {
		List<Document> rules = (List<Document>)  docRules.get("rules");
		int i = 0;
		boolean modified=false;
		if (rules==null){
			rules = new ArrayList<Document>();
		}
		while (i<rules.size()){
			Document docitem =  rules.get(i);
			String key = docitem.entrySet().iterator().next().getKey();
			if (key.equals(rule_id) && modified){
				//to remove items repeated;
				rules.remove(i);
			}else if ((key.equals(rule_id) & !modified)){
				modified = true;
				List<Document> docFields = (List<Document>) docitem.get(rule_id);
				int j = 0;
				while (j<docFields.size()){
					Document docFieldItem = docFields.get(j);
					String pnameIt = docFieldItem.getString("field_name");
					if (pnameIt.equals(param_name)){
						docFieldItem.remove("field_value");
						docFieldItem.append("field_value", param_value);
					}
					j++;
				}
			}
			i++;
		}
		if (!modified){
			List<Document> docFields = new ArrayList<Document>();
			Document doc = new Document();
			doc.append("field_name", param_name);
			doc.append("field_value", param_value);
			docFields.add(doc);
			rules.add(new Document(rule_id,docFields));
		}
		UpdateResult resup =  coll.updateOne(docUser, new Document("$set", new Document("rules",rules)));
		if (resup.getMatchedCount()>0){
			return "OK";
		}else{
			return "ERROR";
		}
	}
	
	

	
	
	

}
