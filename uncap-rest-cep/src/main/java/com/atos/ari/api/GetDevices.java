package com.atos.ari.api;

import java.io.StringReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

@Path("getDevices")
public class GetDevices extends RestOperations {
	final static Logger logger = Logger.getLogger(GetDevices.class);	


    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getIt(@PathParam("user_id") String userName) {
        return "{\"result\""+"= \"Got it!\"}";
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getDevices(String message) {
		jreader = new JsonReader(new StringReader(message));
        jsonObj = (JsonObject) jsonparser.parse(jreader);
        String user_id = jsonObj.get("user_id").getAsString();
        logger.debug("user_id= " + user_id);
        String res=utilities.getDevices(user_id);
        logger.debug(res);
        Response response = buildResponse(res);
        return response;
    }
    
    
    
}
