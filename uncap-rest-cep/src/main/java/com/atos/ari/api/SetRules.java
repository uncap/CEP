package com.atos.ari.api;

import java.io.StringReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

@Path("setRules")
public class SetRules extends RestOperations {
	final static Logger logger = Logger.getLogger(SetRules.class);	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response setRules(String message) {
		jreader = new JsonReader(new StringReader(message));
        jsonObj = (JsonObject) jsonparser.parse(jreader);
        String user_id = jsonObj.get("user_id").getAsString();
        String device_id = jsonObj.get("device_id").getAsString();
        String rule_id = jsonObj.get("rule_id").getAsString();
        String param_name = jsonObj.get("field_name").getAsString();        
        String param_value = jsonObj.get("field_value").getAsString(); 
        logger.debug("user_id= " + user_id);
        logger.debug("device_id= " + device_id);
        logger.debug("rule_id="+ rule_id);
        String res=utilities.setRules(user_id, device_id, rule_id, param_name, param_value);
        logger.debug(res);
        Response response = buildResponse(res);      
        return response;
	}

	

}
