package com.atos.ari.api;

import javax.annotation.security.PermitAll;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.atos.ari.Utilities;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class RestOperations {
	final static Logger logger = Logger.getLogger(RestOperations.class);	
    JsonParser jsonparser = new JsonParser();    
    JsonObject jsonObj = null;
    JsonReader jreader = null;
    Utilities utilities = new Utilities();
    
    protected Response buildResponse(String message){
        Response response = Response.status(200).entity(message)
        													.header("Access-Control-Allow-Origin", "*")
        													.header("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, UPDATE, OPTIONS")
        													.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Key, Authorization").build();
        return response;
    }
    
    @OPTIONS
    @PermitAll
    public Response options() {
        return Response.status(Response.Status.NO_CONTENT)
								.header("Access-Control-Allow-Origin", "*")
								.header("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, UPDATE, OPTIONS")								
        						.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Key, Authorization").build();								
    }

    // Match sub-resources
    @OPTIONS
    @Path("{path:.*}")
    @PermitAll
    public Response optionsAll(@PathParam("path") String path) {
        return Response.status(Response.Status.NO_CONTENT)
								.header("Access-Control-Allow-Origin", "*")
								.header("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, UPDATE, OPTIONS")								
        						.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Key, Authorization").build();								
    }    
}


