package com.atos.ari.api;

import java.io.StringReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

@Path("getRules")
public class GetRules extends RestOperations{
	final static Logger logger = Logger.getLogger(GetRules.class);	

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response  getRules(String message, @Context HttpHeaders header) {
		jreader = new JsonReader(new StringReader(message));
        jsonObj = (JsonObject) jsonparser.parse(jreader);
        String user_id = jsonObj.get("user_id").getAsString();
        String device_id = jsonObj.get("device_id").getAsString();
        logger.debug("user_id= " + user_id);
        logger.debug("device_id= " + device_id);        
        String res=utilities.getRules(user_id, device_id);
        if (res==null){
        	res = "{}";
        }
        logger.debug(res);
        Response response = buildResponse(res);
        return response;
	}


}
