package com.atos.ari;

import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class RestCepTest {

    private HttpServer server;
    private WebTarget target;
	public static Properties config = null;    
	final static Logger logger = Logger.getLogger(RestCepTest.class);	

    @Before
    public void setUp() throws Exception {
        // start the server
    	StartRestInterface.BASE_URI= Configuration.getInstance().config.getProperty("url");
        server = StartRestInterface.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // --
        // c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(StartRestInterface.BASE_URI);
    }


	@After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void testGetDevices() {
    	printInitTest("testGetDevices");
    	String params="{\"user_id\":\"Miguel Rodríguez\"}";
        Response responseMsg = target.path("getDevices").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(params,MediaType.APPLICATION_JSON));
        String res = responseMsg.readEntity(String.class);
        logger.debug("---Result---\n\n");        
        logger.debug(res);
        Assert.assertTrue(res!=null);
    }
    


	/**
     * Test to see that the message "Got it!" is sent in the response.
     */
    
    @Test
    public void testGetRules() {
    	printInitTest("testGetRules");    	
    	String params="{\"user_id\":\"Miguel Rodríguez\",\"device_id\":\"blood_glucose\"}";
        Response responseMsg = target.path("getRules").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(params,MediaType.APPLICATION_JSON));
       String res = responseMsg.readEntity(String.class);
       logger.debug("---Result---\n\n");
       logger.debug(res);
        Assert.assertTrue(res!=null);
    }
    
    @Test
    public void testSetRules() {
    	printInitTest("testSetRules");    	
    	String params="{\"user_id\":\"Miguel Rodríguez\",\"device_id\":\"blood_glucose\",\"rule_id\":\"blood_glucose_high\", \"field_name\":\"maximum\", \"field_value\":887 }";
        Response responseMsg = target.path("setRules").request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(params,MediaType.APPLICATION_JSON));
        String res = responseMsg.readEntity(String.class);
        logger.debug("---Result---\n\n");        
        logger.debug(res);
        Assert.assertTrue(res!=null);
    }
    
    private void printInitTest(String cad) {
    	logger.debug("**********************************************************************");
    	logger.debug("**********************************************************************");
    	logger.debug("                                " + "Test " + cad);
    	logger.debug("**********************************************************************");
    	logger.debug("**********************************************************************");
		
	}
    
    
}
