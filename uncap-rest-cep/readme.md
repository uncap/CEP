# Description

This folder contains the code of the UNCAP API Rest. These operations are
focused on devices and rules of CEP.

# Used by
UNCAP project
# How-to
## Requirements
API uses maven and ant to build the project.

JDK 1.5

maven 3.3

ant 1.9

mongo 3.2

##Compile, build and execute

to compile the project:  mvn compile

to build the jar and execute junit tests: mvn package

to build the eclipse project: mvn eclipse:eclipse and import the project from eclipse.

build.xml is to create the distribution (jar + lib jars).
API runs from eclipse as a Java Application by the StartRestInterface class


# API description
## getDevices
POST /cep-api/getDevices
```
{"user_id":"xxxxx"}
```
response:
```
{ "devices" : ["blood_glucose", "heart_rate", "oxygen_saturation"] }
```
## get Rules
POST /cep-api/getRules
```
{
  "user_id":"xxxx",
  "device_id":"blood_glucose"
}
```

response: 
```
{
  "_id": {
    "$oid": "574560b1d5ae796d008db024"
  },
  "user_id": "Miguel Rodríguez",
  "device_id": "blood_glucose",
  "rules": [
    {
      "blood_glucose_high": [
        {
          "field_name": "maximum",
          "field_value": "300"
        }
      ]
    }
  ]
}
```
## getRules
POST /cep-api/setRules
```
{
  "user_id":"{{userId}}",
  "device_id":"blood_glucose",
  "rule_id":"blood_glucose_high",
    "field_name":"maximum",
  "field_value":"300"
}
```
response: OK or error




# License
Apache 2.0
