# Description

UNCAP (“Ubiquitous iNteroperable Care for Ageing People “) will make use of solutions and technologies developed in previous research
projects to develop an open, scalable and privacy-savvy ICT infrastructure designed to help aging people live independently while 
maintaining and improving their lifestyle. The final solution will consist of real products that will be made available on the market.

# Architecture

<img src="core-cep/doc/architecture.png" width="500" alt="architecture"/>


You can read the Architecture Uncap Deliverable inside the following [link Uncap Architecture] (core-cep/doc/D.1.3_System_Architecture_v2.0.pdf). 
9.6.4 (CEPModule) and 11.23 (Rule management) sections are focused on CEP.

# Modules:

* Complex Event Processing: CEP in the UNCAP cloud works from the “Event detection as a service” perspective.
Its target is providing stream analytic services to components that don’t have resources or are incompatible for
hosting and event processing solution. 
* CEP REST API for managing rules customized by patient.
* CEP Uncap Adaptor to parse messages from Data broker module


# Software requirements
* JDK 1.7 or later
* mongo 3.2.8 or later

# How to use
* You can use the REST API with the postman file inside the [documentation folder] (https://gitlab.com/uncap/CEP/tree/master/core-cep/doc).  The host is uncap.trilogis.it.


