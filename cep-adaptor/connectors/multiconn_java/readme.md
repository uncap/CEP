# Description
This folder contains several connectors in Java language (Kafka, RabbitMQ, Kafka, Mqtt-Mosquitto, Mysql)


# Publishers: 
Publishers are located in "collectors" subfolders with the following:
* HttpPublisher.java
* RabbitmqEventPublisher.java 

# Uncap Adaptor
Specific Uncap Adaptor with the following functions:
* Messages receiving from external MQTT broker (Raptor).
* Message parsing in Uncap Specific Format:
    * UNCAP part parsing. 
    * Open mHealth part parsing.
    * Rules customization by patient via MongoDB.
    * String line CEP event format creation with the message information and the particular data from patients.
* Event sending to CEP via UDP
* Alarm receiving from CEP via UDP
* Alarm sending to external MQTT broker (Raptor)
Main class is com.atos.ari.StartUncapAdaptor
 

# How-to: to describe how to build the project
The project is managed by maven 3: These are the most important commands, they must be executed in the folder where pom.xml is:
* mvn compile; compile the source code.
* mvn package: creates the jar file.
* mvn eclipse:eclipse: creates de eclipse project (.project). This project can be imported from eclipse
* In the protocols/config folder, the config.properties file is used to configure the publishers and collectors:  
Example:  
collectorClass=com.atos.ari.collectors.KafkaEventCollector  
publisherClass=com.atos.ari.publishers.HttpPublisher  
Also there are specific parameters by protocol.
* The connection used for connecting to CEP currently is by UDP but it could be easy to add the connection by MQTT.


# License
multiconn_java for CEP is released as OpenSource with a GPL license. 
It has been fully designed considering Free Open Source Software Technologies.