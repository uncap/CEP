package com.atos.ari;

import java.io.FileInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.atos.ari.event.ComplexEvent;

public class CEPMysqlBridge {
	
	private static Properties config;
	final static Logger logger = Logger.getLogger(CEPMysqlBridge.class);	
	
	
	public static void main(String[] args) throws Exception {
    	System.out.println("Cep Mysql Bridge started");
    	loadConfiguration();
		DatagramSocket serverSocket = new DatagramSocket(Integer.parseInt(getParameter("cep.publisher.port")));
		DataBaseHelper dbh = DataBaseHelper.getInstance(getParameter("database.url"));
		while (true) {
			byte[] receiveData = new byte[1024];			
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			serverSocket.receive(receivePacket);
			String sentence = new String(receivePacket.getData());
			//System.out.println("RECEIVED: " + sentence);
			logger.info("RECEIVED: " + sentence);
			ComplexEvent ce = new ComplexEvent(sentence);
			if (ce.getName()!=null){
				dbh.insertComplexEvent(ce);
			}
			receiveData = null;
		}
	}
	
	
	public static void loadConfiguration() throws Exception {
		config = new Properties();
		config.load(new FileInputStream("./config/config.properties"));
		PropertyConfigurator.configure("./config/log4j.properties");
		logger.info("the configuration loaded is ");
		logger.info(config.toString());		
	}
	
	public static String getParameter(String name){
		return config.getProperty(name);
	}
	
	
	
	
}
