package com.atos.ari;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import com.atos.ari.event.Attribute;
import com.atos.ari.event.ComplexEvent;




public class DataBaseHelper {
	
	private Logger logger = Logger.getLogger("DataBaseHelper");
	private static DataBaseHelper instance = null;
	private static String mysql_odbc;
	
	

	private DataBaseHelper() {
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();			
		}catch (Exception e){
			logger.error("Error getting Data base connection " + e);
			instance = null;
		}
	}
	
	public static DataBaseHelper getInstance(String uri){
		mysql_odbc = uri;
		if (instance == null){
			instance = new DataBaseHelper();
			return instance;
		}else{
			return instance;
		}
	}
	

	
	public boolean testDB() throws Exception{
		Connection conn = DriverManager.getConnection(mysql_odbc);		
		boolean res = false;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "select * from Complex_Event";
		try{
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()){
				System.out.println(rs.getString("name"));
			}
			return true;
		}catch (Exception e){
			logger.error(e);
		}finally{
			try{
				rs.close();
				stmt.close();
				conn.close();
			}catch (Exception e){
				logger.error(e);
			}
		}
		return res;
		
		
	}
	
	public void insertComplexEvent(ComplexEvent ce)  {

		PreparedStatement ps = null;
		String insertIntoComplexEvent = 
				"insert into Complex_Event (id, name) values (?,?)";
		Connection conn = null;
		try{
			conn = DriverManager.getConnection(mysql_odbc);
			int id = getLastId(conn) + 1;
			ps = conn.prepareStatement(insertIntoComplexEvent); 
			ps.setInt(1, id);
			ps.setString(2, ce.getName());
			ps.executeUpdate();
			insertAttributes(id, ce.getAttributes(), conn);

		}catch (Exception e){
			logger.error(e);
		}finally{
			try{
				ps.close();
				if (conn!=null){
					conn.close();
				}
			}catch (Exception e){
				logger.error(e);
			}
		}

		
	}
	
	
    private void insertAttributes(int complex_event_id, List<Attribute> attributes,
			Connection conn) {
		String insertIntoAttributes = 
				"insert into Attribute (name,value,type, complex_event_id) values (?,?,?,?)";
		try{
			int i = 0;
			while (i<attributes.size()){
				Attribute att = attributes.get(i);
				PreparedStatement ps = null;				
				ps = conn.prepareStatement(insertIntoAttributes); 
				ps.setString(1, att.getName());
				ps.setString(2, att.getValue());
				ps.setString(3, att.getType());
				ps.setInt(4, complex_event_id);				
				ps.executeUpdate();
				ps.close();
				i++;
			}
		}catch (Exception e){
			logger.error(e);
		}finally{
			try{
				if (conn!=null){
					conn.close();
				}
			}catch (Exception e){
				logger.error(e);
			}
		}
		

		
	}

	private int getLastId(Connection conn) {
		int res = -1;
		Statement stmt = null;
		ResultSet rs = null;
		String query = "SELECT MAX(id) FROM Complex_Event";
		try{
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			if (rs.next()){
				res = rs.getInt(1);
			}else{
				res=0;
			}
		}catch (Exception e){
			logger.error(e);
		}finally{
			try{
				rs.close();
				stmt.close();
			}catch (Exception e){
				logger.error(e);
			}
		}
		return res;
	}

	public static void main(String[] args ) throws Exception{
    	DataBaseHelper dbhelper =  DataBaseHelper.getInstance("jdbc:mysql://5.79.79.166/cepddbb?user=cepddbb&password=C#hsz.33&autoReconnect=true&failOverReadOnly=false&maxReconnects=10");
    	ComplexEvent ce = new ComplexEvent("heatWave     string datoC hello int datoB 50 int datoA 253");
    	dbhelper.insertComplexEvent(ce);
    	
    	
    	
    }
	
	
	

}
