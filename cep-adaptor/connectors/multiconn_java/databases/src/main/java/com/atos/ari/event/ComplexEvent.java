package com.atos.ari.event;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class ComplexEvent {
	private String name;
	private String cadEvent;
	private List<Attribute> attributes;
	final static Logger logger = Logger.getLogger(ComplexEvent.class);	
	
	public ComplexEvent(String cadEvent) {
		try{
			this.cadEvent = cadEvent.trim();
			String[] cadArr = this.cadEvent.split("\\s+");
			this.name = cadArr[1];		
			int i = 2;
			this.attributes = new ArrayList<Attribute>();
			while (i<cadArr.length){
				String type = cadArr[i++];
				String name = cadArr[i++];
				String value = cadArr[i++]; 
				Attribute at = new Attribute(name,value,type);
				this.attributes.add(at);
			}
		}catch (Throwable t){
			this.name = null;
			logger.error("Error building Complex Event " + t);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	
	
	
	

}
