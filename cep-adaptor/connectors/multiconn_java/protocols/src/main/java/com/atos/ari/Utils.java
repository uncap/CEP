package com.atos.ari;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.atos.ari.event.Attribute;
import com.google.gson.JsonObject;

public class Utils {

	public static void fillWithValues(List<Attribute> attributesInfo,
			JsonObject jobj) {
		int i=0;
		while (i<attributesInfo.size()){
			Attribute att = attributesInfo.get(i);
			String nameAtt = att.getName();
			String value = jobj.get(nameAtt).getAsString();
			value = value.replaceAll("\\s+", "_");
			if (att.getType().equals("bool")){
				att.setValue(boolToInt(value));
				att.setType("int");
			}else{
				att.setValue(value);				
			}

			i++;
		}
	}
	
	private static  String boolToInt(String value) {
		if (value.equalsIgnoreCase("true")){
			return "1";
		}else{
			return "0";
		}
	}
	
	public static String readFile(String path, Charset encoding){
		try{
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
		}catch (Exception e){
			System.out.println(e);
			return null;
		}
	}
	
	

}
