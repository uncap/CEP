package com.atos.ari.uncap.configuration.model;

import java.util.Hashtable;

public class Model {

	private Hashtable<String, Configuration> configurations = new Hashtable<String, Configuration>();

	public void addConfiguration(String key, Configuration config) {
		configurations.put(key, config);
	}

}
