package com.atos.ari.collectors;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import com.atos.ari.EventCollector;
import com.atos.ari.event.Attribute;
import com.atos.ari.event.Event;

public class DummyEventCollector extends EventCollector{

	public DummyEventCollector(Queue<Event> queue) {
		super(queue);
		System.out.println("Initialize connection to the source of the events");
	}

	@Override
	public void collectData() throws Exception {
		//Events simulated
		List<Attribute> attributes = new ArrayList<Attribute>();
		attributes.add(new Attribute("int","value","5"));
		attributes.add(new Attribute("int","sensorId","1"));		
		Event ev1 = new Event("temperature", attributes);
		queue.add(ev1);
		
	}
	

}
