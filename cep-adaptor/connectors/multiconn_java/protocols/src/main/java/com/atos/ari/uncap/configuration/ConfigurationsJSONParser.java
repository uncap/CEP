package com.atos.ari.uncap.configuration;

import java.io.FileReader;

import org.apache.log4j.Logger;

import com.atos.ari.uncap.configuration.model.Configuration;
import com.atos.ari.uncap.configuration.model.Model;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

/**
 * Hello world!
 *
 */
public class ConfigurationsJSONParser 
{
	private String jsonFile="./resources/user_configuration.json";
	final static Logger logger = Logger.getLogger(ConfigurationsJSONParser.class);
    JsonReader jreader = null;
    JsonParser jsonparser = new JsonParser();    
    JsonObject jsonObj = null;    

	public ConfigurationsJSONParser() throws Exception{
		super();
		jreader = new JsonReader(new FileReader(jsonFile));
        jsonObj = (JsonObject) jsonparser.parse(jreader);		
		buildModel();
	}
	
	private void buildModel(){
        JsonElement jsonEl = jsonObj.get("configurations");
        JsonArray jarr =  jsonEl.getAsJsonArray();
        Model model = new Model();
        int i=0;
        while (i<jarr.size()){
        	JsonElement jeItem =   jarr.get(i);
        	JsonObject jobItem =  jeItem.getAsJsonObject();
        	String user_id = jobItem.get("user_id").toString();
        	int high_glucose = Integer.parseInt( jobItem.get("high_glucose").toString());
        	int low_glucose = Integer.parseInt(jobItem.get("low_glucose").toString());
        	int high_heart_rate = Integer.parseInt(jobItem.get("high_heart_rate").toString());
        	int low_heart_rate = Integer.parseInt(jobItem.get("low_heart_rate").toString());
        	Configuration config = new Configuration(user_id, high_glucose, low_glucose, high_heart_rate, low_heart_rate);
        	model.addConfiguration(user_id, config);
        	i++;
        }
		
	}
	
	
	
}
