package com.atos.ari.uncap.openmhealth;

import com.atos.ari.event.Attribute;
import com.atos.ari.event.Event;
import com.atos.ari.uncap.UncapParser;
import com.google.gson.JsonElement;

public class DiastolicBloodPressureCEPParser implements CEPParser {

	public Event parse(JsonElement jsonElPayload, String device, String userId,
			Event event) {
		JsonElement jsonDevice = jsonElPayload.getAsJsonObject().get(device);
		JsonElement jsonEl = jsonDevice.getAsJsonObject(). get("unit");
		String value = jsonEl.getAsString();
		
		event.addAttribute(new Attribute("string","unit",value));
		jsonEl = jsonDevice.getAsJsonObject(). get("value");				
		event.addAttribute(new Attribute("int","value",jsonEl.getAsString()));
	
		UncapParser.fillConfigurationAttributes(event,userId, device);
		return event;
	}

}
