package com.atos.ari.publishers;

import java.util.Queue;

import org.apache.log4j.Logger;

import com.atos.ari.EventPublisher;
import com.atos.ari.StartPublisher;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitmqEventPublisher extends EventPublisher{

	final static Logger logger = Logger.getLogger(RabbitmqEventPublisher.class);
	private Channel channel;
	
	public RabbitmqEventPublisher(Queue<String> queue) {
		super(queue);
		try{
			ConnectionFactory factory = new ConnectionFactory();
		    factory.setHost(StartPublisher.getParameter("rabbit.host"));
	        factory.setPort(Integer.valueOf(StartPublisher.getParameter("rabbit.port")));		    
		    Connection connection = factory.newConnection();
		    this.channel = connection.createChannel();
		    this.channel.queueDeclare(StartPublisher.getParameter("rabbit.queue.publish.name"), false, false, false, null);
		}catch (Exception e){
			System.out.println("Configuration error " + e);
		}
	}

	@Override
	public void publishData() throws Exception {
		String event = queue.poll();
		if (event!=null){
			this.channel.basicPublish(	"",
										StartPublisher.getParameter("rabbit.queue.publish.name"), 
										null,
										event.toString().getBytes());
			logger.info("message published " + event.toString());
		}
	}
	
	
	
	

}
