package com.atos.ari.uncap.configuration.model;

public class Configuration {
	private String user_id;
	private int high_glucose;
	private int low_glucose;
	private int high_hearth_rate;
	private int low_hearth_rate;
	public Configuration(String user_id, int high_glucose, int low_glucose,
			int high_hearth_rate, int low_hearth_rate) {
		super();
		this.user_id = user_id;
		this.high_glucose = high_glucose;
		this.low_glucose = low_glucose;
		this.high_hearth_rate = high_hearth_rate;
		this.low_hearth_rate = low_hearth_rate;
	}
	public String getUser_id() {
		return user_id;
	}
	public int getHigh_glucose() {
		return high_glucose;
	}
	public int getLow_glucose() {
		return low_glucose;
	}
	public int getHigh_hearth_rate() {
		return high_hearth_rate;
	}
	public int getLow_hearth_rate() {
		return low_hearth_rate;
	}
	
	
	
	

}
