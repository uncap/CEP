package com.atos.ari.event;

import java.util.ArrayList;
import java.util.List;

public class Event {
	private String name;
	private List<Attribute> attributes = new ArrayList<Attribute>();
	
	
	public Event(String name, List<Attribute> attributesInfo) {
		super();
		this.name = name;
		this.attributes = attributesInfo;
	}
	
	public Event(){}
	
	public Event(String eventLine){
		String cadEvent = eventLine.trim();
		String[] cadArr = cadEvent.split("\\s+");
		this.name = cadArr[1];		
		int i = 2;
		this.attributes = new ArrayList<Attribute>();
		while (i<cadArr.length){
			String type = cadArr[i++];
			String name = cadArr[i++];
			String value = cadArr[i++]; 
			Attribute at = new Attribute(type,name,value);
			this.attributes.add(at);
		}
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("1 ");		
		builder.append(name);
		builder.append(" ");
		int i = 0;
		while (i<attributes.size()){
			builder.append(attributes.get(i));
			builder.append(" ");
			i++;
		}
		return builder.toString().trim();
	}
	
	public void addAttribute(Attribute att){
		this.attributes.add(att);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	
	
	

}
