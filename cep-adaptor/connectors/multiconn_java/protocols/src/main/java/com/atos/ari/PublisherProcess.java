package com.atos.ari;

import org.apache.log4j.Logger;

public class PublisherProcess extends Thread{
	
	private EventPublisher publisher;
	final static Logger logger = Logger.getLogger(PublisherProcess.class);	
	
	

	public PublisherProcess(EventPublisher publisher) {
		super(publisher.getClass().getName());
		this.publisher = publisher;
	}



	@Override
	public void run() {
		while (true){
			try{
				publisher.publishData();
				Thread.sleep(Long.parseLong(StartPublisher.getParameter("periodForPolling")));				
			}catch (Exception e){
				logger.error("Error collecting data " + e);
			}
		}
	}
	
	

}
