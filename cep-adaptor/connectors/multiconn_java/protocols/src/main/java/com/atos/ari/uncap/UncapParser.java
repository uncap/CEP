package com.atos.ari.uncap;

import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.atos.ari.Utils;
import com.atos.ari.event.Attribute;
import com.atos.ari.event.Event;
import com.atos.ari.event.Message;
import com.atos.ari.uncap.configuration.MongoHelper;
import com.atos.ari.uncap.openmhealth.BodyWeightCEPParser;
import com.atos.ari.uncap.openmhealth.CEPParser;
import com.atos.ari.uncap.openmhealth.DiastolicBloodPressureCEPParser;
import com.atos.ari.uncap.openmhealth.GlucometerCEPParser;
import com.atos.ari.uncap.openmhealth.HearthRateCEPParser;
import com.atos.ari.uncap.openmhealth.OxygenSaturationCEPParser;
import com.atos.ari.uncap.openmhealth.SystolicBloodPressureCEPParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class UncapParser {
	
	final static Logger logger = Logger.getLogger(UncapParser.class);	
	private final static String CURRENT_VALUE = "current-value";
	private final static String CHANNELS = "channels";
	public static final String USER_ID_KEY = "user_id";
	public static final String DOCTOR_ID_KEY = "doctor_id";	
	public static final String DEVICE_ID_KEY = "device_id";
	private static final String HEADER_KEY = "header";	

	
	public static Message format(String message, String[] devices){
		logger.debug("Starting format of message: " + message);
		Message objmessage = new Message();		
		JsonParser jsonparser = new JsonParser();	
		JsonReader jr = new JsonReader(new StringReader(message));
		JsonObject jsonObj  = null;
		JsonElement jsonHeader = null;
		String header = null;
		try{
			jsonObj = (JsonObject) jsonparser.parse(jr);
			jsonHeader = jsonObj.get("header");
			header = jsonHeader.getAsString();			
		}catch (Exception e){
			logger.error("error parsing "+ e);
		}

		logger.debug("Json Header: "  + header );		
		objmessage.getAttsMesagge().put(HEADER_KEY, header);		
		
		JsonObject jsonBody = jsonObj.get("body").getAsJsonObject();
		JsonElement jsonType = jsonBody.get(CHANNELS).getAsJsonObject().get("type").getAsJsonObject().get(CURRENT_VALUE);
		String type = jsonType.getAsString();
		logger.debug("Message type " + type);
		JsonElement jsonUserId = jsonBody.get(CHANNELS).getAsJsonObject().get(USER_ID_KEY).getAsJsonObject().get(CURRENT_VALUE);
		String userId = jsonUserId.getAsString();
		logger.debug("userId: "  + userId );		
		objmessage.getAttsMesagge().put(USER_ID_KEY, userId);
		JsonElement jsonDoctorId = jsonBody.get(CHANNELS).getAsJsonObject().get(DOCTOR_ID_KEY).getAsJsonObject().get(CURRENT_VALUE);
		String doctorId = jsonDoctorId.getAsString();
		logger.debug("doctorId: "  + doctorId );		
		objmessage.getAttsMesagge().put(DOCTOR_ID_KEY, doctorId);
		JsonElement jsonElPayload = jsonBody.get(CHANNELS).getAsJsonObject().get("payload").getAsJsonObject().get(CURRENT_VALUE).getAsJsonObject().get("data");
		
		//event.addAttribute(new Attribute("int", "patientId", jsonPatientId.getAsString()));
		
		//JsonElement jsonElPayload = jsonObj.get("payload");
		//JsonElement jsonElPayload = getJsonElementPayload(payload);
		
		Set<Entry<String, JsonElement>>  entrySet = jsonElPayload.getAsJsonObject().entrySet();
		Iterator<Entry<String, JsonElement>> it = entrySet.iterator(); 
		//the first key has to be the device_id
		while  (it.hasNext()){
			Event event = new Event();
			String deviceJson = it.next().getKey();
			String device = getDeviceMatching(devices, deviceJson);
			if (device!=null){
				event.setName(device);
				CEPParser parser = null;
				if (device.equals("blood_glucose")){
					parser = new GlucometerCEPParser();
				}else if (device.equals("heart_rate")){
					parser = new HearthRateCEPParser();
				}else if (device.equals("oxygen_saturation")){
					parser = new OxygenSaturationCEPParser();
				}else if (device.equals("body_weight")){
					parser = new BodyWeightCEPParser();				
				}else if (device.equals("systolic_blood_pressure")){
					parser = new SystolicBloodPressureCEPParser();				
				}else if (device.equals("diastolic_blood_pressure")){
					parser = new DiastolicBloodPressureCEPParser();				
				}else{
					logger.debug("Type of message is not for CEP");
				}
				if (parser != null){
					event = parser.parse(jsonElPayload, device, userId, event);				
				}
				objmessage.getCepEvents().add(event);
			}
		}
		return objmessage;
	}

	/**
	 * Queries Mongo DB to get configuarion parameters by user and device.
	 * @param event
	 * @param userId
	 * @param device
	 */
	public static void fillConfigurationAttributes(Event event, String userId,String device)	{
		try{
			MongoHelper mh = MongoHelper.getInstance();
			List<Attribute>  atts =  mh.getConfigurationFields(userId, device);
			if (atts != null == !atts.isEmpty()){
				logger.debug("The attributes are the following : " + atts.toString());				
			}else{
				logger.debug ("No attributes for userid = " + userId + " and device " + device);
			}

			int i = 0;
			while (i<atts.size()){
				event.addAttribute(atts.get(i));
				i++;
			}
		}catch (Exception e){
			logger.error("Error filling Configuration Customized " + e + "no attributes will be filled");
		}
	}

	private static JsonElement getJsonElementPayload(String payload) {
		JsonParser jsonparser = new JsonParser();
		JsonReader jr = new JsonReader(new StringReader(payload));
		return  jsonparser.parse(jr);
	}

	private static String getDeviceMatching(String[] devices, String device) {
		int i = 0;
		while (i<devices.length){
			String deviceItem = devices[i].trim();
			if (deviceItem.equalsIgnoreCase(device.trim())){
				return device;
			}
			i++;
		}
		return null;
	}

	public String formatComplexEvent(String complexEventName, String smessage) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("	\"cep_output\": \""+ complexEventName + "\",");
		sb.append("	\"original_event\":");
		sb.append("		"+ smessage);
		sb.append("}");
		String res = null;
		try{
			JsonParser jsonparser = new JsonParser();
			JsonReader jr = new JsonReader(new StringReader(sb.toString()));
			JsonObject jsonObj = (JsonObject) jsonparser.parse(jr);
			res = jsonObj.toString();
		}catch (Exception e){
			logger.error(e);
		}
		return res;
	}

	/*format with json, fails, it build as array
	
	public String formatComplexEvent(String complexEventName, Message objMessage) {
		String userId = objMessage.getAttsMesagge().get(UncapParser.USER_ID_KEY);
		String deviceId = "cep";
		String doctorId = objMessage.getAttsMesagge().get(UncapParser.DOCTOR_ID_KEY);
		String timestamp = String.valueOf(System.currentTimeMillis());
		String type="alarm";
		JsonObject jobj = new JsonObject();
		JsonArray jsonArr = new JsonArray();
		jsonArr.add(buildProperty(UncapParser.USER_ID_KEY, userId));
		jsonArr.add(buildProperty(UncapParser.DEVICE_ID_KEY, deviceId));		
		jsonArr.add(buildProperty(UncapParser.DOCTOR_ID_KEY, doctorId));
		jsonArr.add(buildProperty("timestamp", timestamp));
		jsonArr.add(buildProperty("type", type));
		String payload = complexEventName;
		jsonArr.add(buildProperty("payload", payload));		
		jobj.add("channels", jsonArr);
		return jobj.toString();
	}
	*/
	
	public String formatComplexEvent(String complexEventName, Message objMessage) {
		String userId = objMessage.getAttsMesagge().get(UncapParser.USER_ID_KEY);
		String doctorId = objMessage.getAttsMesagge().get(UncapParser.DOCTOR_ID_KEY);
		String timestamp = String.valueOf(System.currentTimeMillis()/1000);
		//String timestamp = String.valueOf(System.currentTimeMillis());
    	String message = replaceValues(userId, doctorId, timestamp,complexEventName);
    	return message;
		
	}
	
	
	
	private static String replaceValues(String userId, String doctorId,
			String timestamp, String complexEventName) {
		String coplexFormatted = formatComplexEventName(complexEventName);
    	String message=Utils.readFile("./resources/uncap/uncapToBroker.json.template",
    			Charset.forName("UTF-8"));
    	message = message.replace("${user_id}", userId);
    	message = message.replace("${doctor_id}", doctorId);
    	message = message.replace("${timestamp}", timestamp);
    	message = message.replace("${complex_event_name}", coplexFormatted);    	
    	return message;

	}
	
	private static String formatComplexEventName(String value){
		if (value!=null){
			return value.replace("_", " ");
		}else{
			return "alarm";
		}
	}

	private static JsonObject buildProperty (String key, String value){
		JsonObject jobProp = new JsonObject();
		jobProp.addProperty("current-value", value);
		JsonObject item = new JsonObject();
		item.add(key, jobProp);
		return item;
	}
	
	public static void main (String args[]){
		String message = replaceValues("user413241", "doctor43124", "" +System.currentTimeMillis(), "high_glucose");
		System.out.println(message);
	}
	
	
	

}
