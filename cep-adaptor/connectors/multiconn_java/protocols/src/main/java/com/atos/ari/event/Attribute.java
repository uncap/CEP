package com.atos.ari.event;

public class Attribute {
	private String type;
	private String name;
	private String value;
	public Attribute(String type, String name, String value) {
		super();
		this.type = type;
		this.name = name;
		this.value = value;
		if (type.equals("string")){
			this.value = value.replace(" ", "_");
		}

		
	}
	
	private String format() {
		if (value.lastIndexOf(" ")>0){
			value = "\""+value+"\"";			
		}

		return value;
	}

	public Attribute(String type, String name) {
		super();
		this.type = type;
		this.name = name;
		this.value = null;
	}
	
	
	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(type);
		sb.append(" ");
		sb.append(name);
		sb.append(" ");
		sb.append(value);
		return sb.toString();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	
	
	

}
