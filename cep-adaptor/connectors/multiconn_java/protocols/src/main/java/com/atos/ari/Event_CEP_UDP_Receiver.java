package com.atos.ari;

import java.util.Queue;

import com.atos.ari.event.Event;

public class Event_CEP_UDP_Receiver extends Thread{
	
	private Queue<String> queue;
	private UDPServerHelper udpServerHelper;
	

	public Event_CEP_UDP_Receiver(Queue<String> queue) throws Exception {
		super("CEP_UDP_Sender");
		this.queue = queue;
		String host = StartPublisher.getParameter("cep.host");
		String port = StartPublisher.getParameter("cep.publisher.port");
		this.udpServerHelper = new UDPServerHelper(host,port); 
	}



	@Override
	public void run() {
		while (true){
			String sevent = this.udpServerHelper.receiveEventFromCEP_UDP();
			queue.add(sevent);
		}
	}
	

}
