package com.atos.ari.collectors;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Queue;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.atos.ari.EventCollector;
import com.atos.ari.StartCollector;
import com.atos.ari.Utils;
import com.atos.ari.event.Attribute;
import com.atos.ari.event.Event;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;




public class PsocratesEMTEventCollector extends EventCollector{
	
	final static Logger logger = Logger.getLogger(PsocratesEMTEventCollector.class);
	private CloseableHttpClient httpClient;	
	private List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();	
    private HttpPost post = new HttpPost(StartCollector.getParameter("url"));
    List<String> stopIds;

	public PsocratesEMTEventCollector(Queue<Event> queue) throws Exception {
		super(queue);
        stopIds = getStopIds();		
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        FileInputStream instream = new FileInputStream(new File("./config/emtkeystore.jks"));
        try{
        	trustStore.load(instream, "123456".toCharArray());
       	}finally{
       		instream.close();
       	}
        //Trust on CA and all self-signed certs
        SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy()).build();
        //Allow TLSv1 protocol only
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(	sslcontext, 
        																	new String[] {"TLSv1"},
        																	null,
        																	SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        this.httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        HttpClientBuilder hcb =  HttpClients.custom();
        hcb.setSSLSocketFactory(sslsf);
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30 * 1000).build();        
        hcb.setDefaultRequestConfig(requestConfig);
        this.httpClient = hcb.build();
        


	}
	
	public List<Event> getResponse(int stopId){
		List<Event> res = null;		
        InputStreamReader isr = null;
        JsonReader jreader = null;
		try{
	        this.nameValuePairs = new ArrayList<BasicNameValuePair>();
	        addCommonParameters();
	        this.nameValuePairs.add(new BasicNameValuePair("idStop", String.valueOf(stopId)));
	        this.nameValuePairs.add(new BasicNameValuePair("DateTime_Referenced_Incidencies_YYYYMMDD", getCurrentDate()));
	        this.post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        HttpResponse response = httpClient.execute(this.post);
	        //BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	        JsonParser jsonparser = new JsonParser();
	        isr = new InputStreamReader(response.getEntity().getContent());
	        jreader = new JsonReader(isr);
	        JsonObject jsonObj = (JsonObject) jsonparser.parse(jreader);
	        res = buildList(jsonObj);
		}catch(Throwable e){
			logger.error("Error processing Response from EMT " + e);
		}finally{
			if (jreader!=null){
				try{
			        jreader.close();					
				}catch (Exception e){}
			}
			if (isr!=null){
				try{
					isr.close();					
				}catch (Exception e){}
			}
		}
        return res;


	}
	private List<Event> buildList(JsonObject jsonObj) {
		List<Event> res = new ArrayList<Event>();
        String incidenceDescription = null;
        JsonElement jsonElArrives = jsonObj.get("arrives");
        JsonElement jsonIncident = jsonObj.get("incident");
        JsonElement jsonIncidentList = jsonIncident.getAsJsonObject().get("incidentList");
        if (jsonIncidentList != null){
            JsonElement jsonIncidentData = jsonIncidentList.getAsJsonObject().get("data");
            JsonElement jsonIncidentDescription = jsonIncidentData.getAsJsonObject().get("description"); 
            incidenceDescription = jsonIncidentDescription.getAsString();
            incidenceDescription = incidenceDescription.substring(0, 39);
            incidenceDescription = incidenceDescription.replaceAll(" ", "_");
            //incidenceDescription = "incidencieDescription";
            
        }
        
        JsonElement jsonElArrEstimation = jsonElArrives.getAsJsonObject().get("arriveEstimationList");
        JsonElement jsonArrive = jsonElArrEstimation.getAsJsonObject().get("arrive");        
        JsonArray jsonArr=jsonArrive.getAsJsonArray();
       
        
        int i = 0;
        while (i<jsonArr.size()){
        	JsonObject jobj = jsonArr.get(i).getAsJsonObject();        	
        	Event event;
        	List<Attribute> attributesInfo = initializeAttributes();        	
    		//event = new Event(StartCollector.getParameter("event.config.name"), jobj, attributesInfo);
        	Utils.fillWithValues(attributesInfo, jobj);
        	event = new Event(StartCollector.getParameter("event.config.name"), attributesInfo);
    		if (incidenceDescription != null){
        		Attribute attIncidenceDesc = new Attribute("string", "incidenceDesc", incidenceDescription);
        		event.addAttribute(attIncidenceDesc);
    		}
    		Attribute attIncidence;    		
        	if (i==1 && incidenceDescription!=null){
        		attIncidence = new Attribute("int", "incidence", "1");
        	}else{
        		attIncidence = new Attribute("int", "incidence", "0");        		
        	}
        	event.addAttribute(attIncidence);
        	res.add(event);
        	i++;
        }
		return res;
	}

	private List<Attribute> initializeAttributes() {
		String propAtt = StartCollector.getParameter("event.config.attributes");
		String[] propAttArr = propAtt.split(",");
		List<Attribute> res = new ArrayList<Attribute>();
		int i=0;
		while (i<propAttArr.length){
			String item = propAttArr[i].trim();
			String[] itemArr = item.split("-");
			Attribute att = new Attribute(itemArr[0].trim(), itemArr[1].trim());
			res.add(att);
			i++;
		}
		return res;
	}

	private String getCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(new Date());
	}

	public void addCommonParameters(){
        this.nameValuePairs.add(new BasicNameValuePair("op", "GetEstimatesIncident"));        
        this.nameValuePairs.add(new BasicNameValuePair("idClient", StartCollector.getParameter("emtcredential.idClient")));
        this.nameValuePairs.add(new BasicNameValuePair("passKey", StartCollector.getParameter("emtcredential.password")));
        this.nameValuePairs.add(new BasicNameValuePair("Text_StopRequired_YN", "Y"));
        this.nameValuePairs.add(new BasicNameValuePair("Audio_StopRequired_YN", "Y"));
        this.nameValuePairs.add(new BasicNameValuePair("Text_EstimationsRequired_YN", "Y"));
        this.nameValuePairs.add(new BasicNameValuePair("Text_IncidencesRequired_YN", "Y"));
        this.nameValuePairs.add(new BasicNameValuePair("Audio_IncidencesRequired_YN", "Y"));
	}
	
	
	private static List<String> getStopIds() {
		String stopIdsStr = StartCollector.getParameter("emt.stop.ids");
		String[] stopIdsArr = stopIdsStr.split(",");
		return Arrays.asList(stopIdsArr);
	}


	public void collectData() throws Exception {
		int i = 0;
    	while (i<stopIds.size()){
            List<Event> resp = getResponse(Integer.valueOf(stopIds.get(i)));
            if (resp!=null){
            	queue.addAll(resp);  
            }
        	Thread.sleep(Long.parseLong(StartCollector.getParameter("periodBetweenWSCalls")));
    		i++;
    	}
	}

	

}
