package com.atos.ari.uncap.openmhealth;

import com.atos.ari.event.Event;
import com.google.gson.JsonElement;

public interface CEPParser {
	public Event  parse(JsonElement jsonElPayload, String device, String userId, Event event);
}
