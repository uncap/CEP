package com.atos.ari.mqtt;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class MqttHelper implements MqttCallback{
	
	private String inputTopic;
	private String outPutTopic;
	private int qos; //quality of service
	private String urlbroker; //example:tcp://localhost:1883
    private String clientId     = "uncapCEP";	
    private String user;
    private String password;
    private MqttClient mqttClient = null;
    //private MqttAsyncClient mqttAsincClient = null;    
	final static Logger log = Logger.getLogger(MqttHelper.class);
	public  Queue<String> queue = new ConcurrentLinkedQueue<String>();	
	//public  Queue<String> queue = new ArrayBlockingQueue<String>(10);
	
    

	public MqttHelper(String inputTopic, String outPutTopic, int qos,
			String urlbroker, String clientId, String user, String password) {
		super();
		this.inputTopic = inputTopic;
		this.outPutTopic = outPutTopic;
		this.qos = qos;
		
		this.urlbroker = urlbroker;
		this.clientId = clientId;
		this.user=user;
		this.password = password;
 	}
	
	public void connect(){
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            this.mqttClient = new MqttClient(this.urlbroker, clientId, persistence); 
            this.mqttClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            //connOpts.setKeepAliveInterval(30);
            if (this.user != null && this.password != null){
                connOpts.setUserName(this.user);
                connOpts.setPassword(this.password.toCharArray());           
            }
            log.info("Connecting to broker: "+this.urlbroker);
            this.mqttClient.connect(connOpts);
            log.info("Connected");
            this.mqttClient.subscribe(inputTopic);
        } catch(Exception e) {
        	log.error(e);
        }
	}

	public String get(){
		log.debug("MQTT messages in queue: " + queue.size());
		try{
			return queue.poll();			
		}catch (Throwable e){
			log.error("error getting message from queue " + e);
			return null;
		}

	}
	
	public void publish(String message){
		try{
	        log.debug("Publishing message: \n"+message);
	        log.debug("Publishing in " + this.urlbroker + " - topic: " + this.outPutTopic);
	        MqttMessage mqttMess = new MqttMessage(message.getBytes());
	        mqttMess.setQos(this.qos);
	        mqttClient.setTimeToWait(3000);
	        try{
		        mqttClient.publish(this.outPutTopic, mqttMess);
		        log.debug("Published in " + this.urlbroker + " - topic: " + this.outPutTopic);		        
	        }catch (Throwable t){
		        log.error("Message Not published " + t);	        	
	        }
		}catch (Exception e){
			log.error(e);
		}
	}
	
	public void publish(String message, String topic){
		try{
	        log.debug("Publishing message: \n"+message);
	        log.debug("Publishing in " + this.urlbroker + " - topic: " + topic);
	        MqttMessage mqttMess = new MqttMessage(message.getBytes());
	        mqttMess.setQos(this.qos);
	        mqttClient.setTimeToWait(3000);
	        try{
		        mqttClient.publish(topic, mqttMess);
		        log.debug("Published in " + this.urlbroker + " - topic: " + topic);		        
	        }catch (Throwable t){
		        log.error("Message Not published " + t);
	        }
		}catch (Exception e){
			log.error(e);
		}
	}
	
	
	public void disconnect(){
		try{
	        mqttClient.disconnect();
	        log.debug("Disconnected");
		}catch (Exception e){
			log.error(e);
		}
	}
	
	public void connectionLost(Throwable t) {
		log.error("Connection lost " + t);
			boolean connected = false;
			while (!connected){
				try{
					if (mqttClient.isConnected()){
						mqttClient.disconnect();				
					}
					mqttClient.connect();
					connected = true;
					log.debug("Reconnected");
				}catch (Exception e){
					log.error("Error trying to reconnect, next try in 30 seconds" );
					try{
						Thread.sleep(30000);						
					}catch (Exception ie){
						log.error(ie);
					}
				}
			}
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		try{
			//log.debug("Pub complete" + new String(token.getMessage().getPayload()));
			log.debug("Pub complete");
		}catch (Exception e){
			log.error(e);
		}

		
	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {
		//message.setRetained(false);
		String smessage = new String(message.getPayload());
		log.debug("-------------------------------------------------");
		log.debug("--------Messge arrived-------------");
		log.debug("--------" + "From " +  this.urlbroker  + "-------------");		
		//log.debug("| ID:" + message.);		
		log.debug("| Topic:" + topic);
		log.debug("| Message: " + smessage);
		log.debug("-------------------------------------------------");		
		queue.add(smessage);
		return;

	}
	
	public boolean isConnected(){
		return (mqttClient != null && mqttClient.isConnected());
	}
	

}
