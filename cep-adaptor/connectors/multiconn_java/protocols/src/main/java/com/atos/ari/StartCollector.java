package com.atos.ari;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.atos.ari.collectors.KafkaEventCollector;
import com.atos.ari.event.Event;

public class StartCollector 
{
	final static Logger logger = Logger.getLogger(StartCollector.class);	
	public static Properties config;
	public static Queue<Event> queue = new ConcurrentLinkedQueue<Event>();	
	
    public static void main( String[] args ) throws Exception {
    	System.out.println("Event Collector started");
        loadConfiguration();    	
    	
        EventCollector evtCollector = FactoryCollector.getEventCollector(queue);
        CollectorProcess collectorData = new CollectorProcess(evtCollector);
        collectorData.start();
        
        Event_CEP_UDP_Sender senderToCep = new Event_CEP_UDP_Sender(StartCollector.queue);
        senderToCep.start();
        
    }



	private static void loadConfiguration() throws Exception {
		config = new Properties();
		config.load(new FileInputStream("./config/config.properties"));
		PropertyConfigurator.configure("./config/log4jCollector.properties");
		logger.info("the configuration loaded is ");
		logger.info(config.toString());		
	}
	
	public static String getParameter(String name){
		return config.getProperty(name).trim();
	}
}
