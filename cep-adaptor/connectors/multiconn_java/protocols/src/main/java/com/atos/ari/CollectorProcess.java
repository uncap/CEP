package com.atos.ari;

import org.apache.log4j.Logger;

public class CollectorProcess extends Thread{
	
	private EventCollector collector;
	final static Logger logger = Logger.getLogger(CollectorProcess.class);	
	
	

	public CollectorProcess(EventCollector collector) {
		super(collector.getClass().getName());
		this.collector = collector;
	}



	@Override
	public void run() {
		while (true){
			try{
				collector.collectData();
				Thread.sleep(Long.parseLong(StartCollector.getParameter("periodForPolling")));				
			}catch (Exception e){
				logger.error("Error collecting data " + e);
			}
		}
	}
	
	

}
