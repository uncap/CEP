package com.atos.ari.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Message {
	private Map<String,String> attsMesagge = new HashMap<String, String>();
	private List<Event> cepEvents= new ArrayList<Event>();
	
	public Map<String, String> getAttsMesagge() {
		return attsMesagge;
	}
	public void setAttsMesagge(Map<String, String> attsMesagge) {
		this.attsMesagge = attsMesagge;
	}
	public List<Event> getCepEvents() {
		return cepEvents;
	}
	public void setCepEvents(List<Event> cepEvents) {
		this.cepEvents = cepEvents;
	}
	
	
}
