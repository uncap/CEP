package com.atos.ari.publishers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.util.Queue;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.log4j.Logger;

import com.atos.ari.EventPublisher;
import com.atos.ari.event.Attribute;
import com.atos.ari.event.Event;

public class HttpPublisher extends EventPublisher {
	
	final static Logger log = Logger.getLogger(HttpPublisher.class);	

	public HttpPublisher(Queue<String> queue) throws Exception {
		super(queue);
	}

	@Override
	public void publishData() throws Exception {
		
		String event = queue.poll();
		if (event!=null){
			sendHttpEvent(event);
		}
	}

	private void sendHttpEvent(String event) {
		String urlTail = buildUrlTail(event);
		//String https_url = "https://dweet.io/dweet/for/cep?complexEvent=heatWave&sensorId=45&temperature=50";
		String https_url = "https://dweet.io/dweet/for/cep?" + urlTail;
		URL url;
		try {

			url = new URL(https_url);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			// dumpl all cert info
			//print_https_cert(con);

			// dump all the content
			print_content(con);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String buildUrlTail(String event) {
		Event evobj = new Event(event);
		StringBuilder sb = new StringBuilder();
		sb.append("complexEvent");sb.append("=");sb.append(evobj.getName());
		if (evobj.getAttributes()!=null && !evobj.getAttributes().isEmpty()){
			sb.append("&");
			int i=0;
			while (i<evobj.getAttributes().size()){
				Attribute att = evobj.getAttributes().get(i);
				sb.append(att.getName());sb.append("=");sb.append(att.getValue());
				if (i < (evobj.getAttributes().size() -1)){
					sb.append("&");					
				}
				i++;
			}
		}
		return sb.toString();
	}

	private void print_https_cert(HttpsURLConnection con) {

		if (con != null) {

			try {

				log.info("Response Code : " + con.getResponseCode());
				log.info("Cipher Suite : " + con.getCipherSuite());
				log.info("\n");

				Certificate[] certs = con.getServerCertificates();
				for (Certificate cert : certs) {
					log.info("Cert Type : " + cert.getType());
					log.info("Cert Hash Code : " + cert.hashCode());
					log.info("Cert Public Key Algorithm : "
							+ cert.getPublicKey().getAlgorithm());
					log.info("Cert Public Key Format : "
							+ cert.getPublicKey().getFormat());
					log.info("\n");
				}

			} catch (SSLPeerUnverifiedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	private void print_content(HttpsURLConnection con) {
		if (con != null) {

			try {

				log.info("****** Content of the URL ********");
				BufferedReader br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));

				String input;

				while ((input = br.readLine()) != null) {
					log.info(input);
				}
				br.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
