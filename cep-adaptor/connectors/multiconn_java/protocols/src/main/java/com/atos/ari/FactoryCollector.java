package com.atos.ari;

import java.util.Queue;

import org.apache.log4j.Logger;

import com.atos.ari.event.Event;

public class FactoryCollector {
	final static Logger logger = Logger.getLogger(FactoryCollector.class);	
	public static EventCollector getEventCollector(Queue<Event> queue){
		try{
			Class<Queue<Event>> collectorClass = (Class<Queue<Event>>) Class.forName(StartCollector.getParameter("collectorClass"));
			EventCollector collObj = (EventCollector)collectorClass.getDeclaredConstructor(Queue.class).newInstance(queue);	 
			return (EventCollector) collObj;			
		}catch (Exception e){
			logger.error(e);
			return null;
		}
	}

}
