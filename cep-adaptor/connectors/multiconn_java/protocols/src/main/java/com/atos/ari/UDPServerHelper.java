package com.atos.ari;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

import org.apache.log4j.Logger;

public class UDPServerHelper {
	private String host;
	private String port;
	private DatagramSocket serverSocket = null;
	private InetAddress ipAddress = null;
	final static Logger logger = Logger.getLogger(UDPServerHelper.class);
	
	public UDPServerHelper(String host, String port) throws Exception{
		super();
		this.host = host;
		this.port = port;
		ipAddress = InetAddress.getByName(this.host);		
		serverSocket = new DatagramSocket(Integer.parseInt(this.port), ipAddress);

	}
	
	public String receiveEventFromCEP_UDP() {
		try{
			byte[] receiveData = new byte[1024];			
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			serverSocket.receive(receivePacket);
			String event = new String(receivePacket.getData());
			logger.info("Event received: "+ event);
			return event;
		}catch (Throwable e){
			logger.error("Error Sending Event " + e);
		}
		return null;
	}
	
	public String receiveEventFromCEP_UDP(int timeout) {
		try{
			byte[] receiveData = new byte[1024];			
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			serverSocket.setSoTimeout(timeout);
			serverSocket.receive(receivePacket);
			String event = new String(receivePacket.getData());
			logger.info("Event received: "+ event);
			return event;
		}catch (SocketTimeoutException ste){
			logger.debug("Timeout, return null, no event received ");
			return null;
		}catch (Throwable e){
			logger.error("Error Sending Event " + e);
		}
		return null;
	}

}
