package com.atos.ari;

import java.util.Queue;

import com.atos.ari.event.Event;

public class Event_CEP_UDP_Sender extends Thread{
	
	private Queue<Event> queue;
	private UDPClientHelper udpHelper;
	

	public Event_CEP_UDP_Sender(Queue<Event> queue) throws Exception {
		super("CEP_UDP_Sender");
		this.queue = queue;
		String host = StartCollector.getParameter("cep.host");
		String port = StartCollector.getParameter("cep.collector.port");
		this.udpHelper = new UDPClientHelper(host,port); 
	}



	@Override
	public void run() {
		Event event;
		while (true){
			event = queue.poll();
			if (event!=null){
				udpHelper.sendEventToCEP_UDP(event.toString().trim());
			}
		}
	}
	

}
