package com.atos.ari.mqtt;

import java.nio.charset.Charset;

import com.atos.ari.Utils;

public class MqttMessageSimulator {
	
	//public static String mosquittoURL = "tcp://localhost:1883";
	//public static String topicToPublish = "raptorInputSimulator";
	//public static String user = null;
	//public static String password = null;
	
	public static String mosquittoURL = "tcp://uncap.ddns.net:1883";
	public static String user = "uncap-cep";
	public static String password = "g7sdk34f35gh07hg";
	public static String topicToPublish = "system/measurement";
	
	
	public static void main (String args[]) throws Exception{
		MqttPublisher mqttPublisher = new MqttPublisher(mosquittoURL,
														topicToPublish, user, password);
		//String file = "./resources/simulatorMessages/asRaptorMessageGlucometer.json";
		String file = "./resources/simulatorMessages/asRaptorMessageHeartRate.json";		
		//String file = "./resources/simulatorMessages/asRaptorMessageOxygenSaturation.json";
		//String file = "./resources/simulatorMessages/asRaptorMessageWeight.json";
		//String file = "./resources/simulatorMessages/asRaptorMessageBloodPressure.json";		
    	String message=Utils.readFile(file, Charset.forName("UTF-8"));
		while (true){
			mqttPublisher.publish(message);
			Thread.sleep(10000);
		}
		
	}

}
