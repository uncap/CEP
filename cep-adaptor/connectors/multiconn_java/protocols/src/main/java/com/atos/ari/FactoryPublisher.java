package com.atos.ari;

import java.util.Queue;

import org.apache.log4j.Logger;

public class FactoryPublisher {
	final static Logger logger = Logger.getLogger(FactoryPublisher.class);	
	public static EventPublisher getEventPublisher(Queue<String> queue){
		try{
			Class<Queue<String>> publisherClass = (Class<Queue<String>>) Class.forName(StartPublisher.getParameter("publisherClass"));
			EventPublisher collObj = (EventPublisher)publisherClass.getDeclaredConstructor(Queue.class).newInstance(queue);	 
			return (EventPublisher) collObj;			
		}catch (Exception e){
			logger.error(e);
			return null;
		}
	}

}
