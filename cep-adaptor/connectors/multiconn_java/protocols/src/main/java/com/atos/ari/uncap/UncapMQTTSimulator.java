package com.atos.ari.uncap;

import java.nio.charset.Charset;

import com.atos.ari.Utils;
import com.atos.ari.mqtt.MqttHelper;

public class UncapMQTTSimulator {

	public static void main(String[] args) {
		
    	try{
        	MqttHelper mqttHelper = 
        			new MqttHelper(	"uncapInputTopic",
        							"uncapInputTopic",
        							1,
        							"tcp://localhost:1883",
        							"testClientid", null,null);
        	mqttHelper.connect();
        	String message=Utils.readFile("./src/test/resources/glucometer.json",
        			Charset.forName("UTF-8"));
        	while (true){
            	mqttHelper.publish(message);
            	Thread.sleep(1000);            	
        	}
    	}catch (Exception e){
    		System.out.println(e);
    	}
	}

}
