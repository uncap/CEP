package com.atos.ari;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.log4j.Logger;

public class UDPClientHelper {
	private String host;
	private String port;
	private DatagramSocket clientSocket = null;
	private InetAddress ipAddress = null;
	final static Logger logger = Logger.getLogger(UDPClientHelper.class);
	
	public UDPClientHelper(String host, String port) throws Exception{
		super();
		this.host = host;
		this.port = port;
		clientSocket = new DatagramSocket();
		ipAddress = InetAddress.getByName(this.host);
	}
	
	public void sendEventToCEP_UDP(String data) {
		try{
			String dataWithType = data;
			byte[] sendData = dataWithType.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData,
					sendData.length, ipAddress, Integer.parseInt(port));
			clientSocket.send(sendPacket);
			logger.info("Event sent: "+ data);
		}catch (Throwable e){
			logger.error("Error Sending Event " + e);
		}
	}
	

}
