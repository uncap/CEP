package com.atos.ari;

import java.util.Queue;

import com.atos.ari.event.Event;


public abstract class EventCollector {
	
	protected Queue<Event> queue;
	
	public EventCollector(Queue<Event> queue){
		this.queue = queue;
	}
 
	public void collectData() throws Exception{
		System.out.println("Error: You must implement collectData method ");
	};

}
