package com.atos.ari.mqtt;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttPublisher implements MqttCallback{
	
	private String urlBroker;
	private String topicToPublish;
	private static int qos=0;
	final static Logger log = Logger.getLogger(MqttPublisher.class);
    private MqttClient mqttClient = null;	
	
	public MqttPublisher(String urlBroker, String topicToPublish, String user, String password){
		this.urlBroker = urlBroker;
		this.topicToPublish = topicToPublish;
        MemoryPersistence persistence = new MemoryPersistence();
        try {
            this.mqttClient = new MqttClient(this.urlBroker, "simulator", persistence);
            this.mqttClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setKeepAliveInterval(30);
            if (user != null && password != null){
                connOpts.setUserName(user);
                connOpts.setPassword(password.toCharArray());           
            }
            log.info("Connecting to broker: "+this.urlBroker);
            this.mqttClient.connect(connOpts);
            log.info("Connected");
        } catch(Exception e) {
        	log.error(e);
        }
	}

	public void connectionLost(Throwable t) {
		log.error("Connection lost " + t);	}

	public void deliveryComplete(IMqttDeliveryToken arg0) {
		try{
			//log.debug("Pub complete" + new String(token.getMessage().getPayload()));
			log.debug("Pub complete");
		}catch (Exception e){
			log.error(e);
		}
	}
	
	public void publish(String message){
		try{
	        log.debug("Publishing message: \n"+message);
	        log.debug("Publishing in " + this.urlBroker + " - topic: " + this.topicToPublish);
	        MqttMessage mqttMess = new MqttMessage(message.getBytes());
	        mqttMess.setQos(0);
	        mqttClient.publish(this.topicToPublish, mqttMess);            	
		}catch (Exception e){
			log.error(e);
		}
	}
	

	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {
		// Not implemented because it's only to publish
		
	}

}
