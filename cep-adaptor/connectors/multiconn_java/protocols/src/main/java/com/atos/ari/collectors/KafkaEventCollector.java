package com.atos.ari.collectors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;

import com.atos.ari.EventCollector;
import com.atos.ari.StartCollector;
import com.atos.ari.collectors.kafka.KafkaConsumer;
import com.atos.ari.event.Event;

public class KafkaEventCollector extends EventCollector{
	private ConsumerConnector consumer;
	private ExecutorService executor;
	
	private final String a_zookeeper = StartCollector.getParameter("kafka.a_zookeeper");
	private final String a_groupId = StartCollector.getParameter("kafka.a_groupId");
	private final String topic = StartCollector.getParameter("kafka.a_topic");
	private final int threads = Integer.parseInt(StartCollector.getParameter("kafka.threads"));
	
	public KafkaEventCollector(Queue<Event> queue) {
		super(queue);
		
		this.consumer = null;
		// No entiendo por qué si importo kafka.consumer.Consumer, no se encuentra la función
		try{
			this.consumer = kafka.consumer.Consumer.createJavaConsumerConnector(createConsumerConfig(a_zookeeper, a_groupId));			
		}catch (Exception e){
			System.out.println(e);
		}
	}

	private static ConsumerConfig createConsumerConfig(String a_zookeeper, String a_groupId) {
		Properties props = new Properties();
		props.put("zookeeper.connect", a_zookeeper);
		props.put("group.id", a_groupId);
		props.put("zookeeper.session.timeout.ms", "4000");
		props.put("zookeeper.sync.time.ms", "200");
		props.put("auto.commit.interval.ms", "1000"); 
        return new ConsumerConfig(props);
    }
	
	@Override
	public void collectData() throws Exception {
		run(this.threads);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {

		}
		// Uncomment in order the process to be terminated after 10 seconds without receiving
		//is.shutdown();

	}
	
	public void shutdown() {
		if (consumer != null) consumer.shutdown();
			if (executor != null) executor.shutdown();
				try {
					if (!executor.awaitTermination(5000, TimeUnit.MILLISECONDS)) {
						System.out.println("Timed out waiting for consumer threads to shut down, exiting uncleanly");
					}
				} catch (InterruptedException e) {
					System.out.println("Interrupted during shutdown, exiting uncleanly");
				}
	}
	
	public void run(int a_numThreads) {
		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
		topicCountMap.put(topic, new Integer(a_numThreads));
		Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
		List<KafkaStream<byte[], byte[]>> streams = consumerMap.get(topic);
		
		// now launch all the threads
		//
		executor = Executors.newFixedThreadPool(this.threads);
			
		// now create an object to consume the messages
		//
		int threadNumber = 0;
		for (final KafkaStream stream : streams) {
			executor.submit(new KafkaConsumer(stream, threadNumber));
			//threadNumber++;
			
			/*if(threadNumber >= this.threads-1)
				threadNumber = 0;*/
		}
	}
}