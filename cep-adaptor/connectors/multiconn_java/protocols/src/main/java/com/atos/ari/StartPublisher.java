package com.atos.ari;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class StartPublisher {
	
	final static Logger logger = Logger.getLogger(StartPublisher.class);	
	public static Properties config;
	public static Queue<String> queue = new ConcurrentLinkedQueue<String>();	
	
    public static void main( String[] args ) throws Exception {
    	System.out.println("Event Publisher started");
        loadConfiguration();
        
        Event_CEP_UDP_Receiver receiverFromCep = new Event_CEP_UDP_Receiver(StartPublisher.queue);
        receiverFromCep.start();
    	
        EventPublisher evtPublisher = FactoryPublisher.getEventPublisher(queue);
        PublisherProcess publisherProcess = new PublisherProcess(evtPublisher);
        publisherProcess.start();
        
    }



	private static void loadConfiguration() throws Exception {
		config = new Properties();
		config.load(new FileInputStream("./config/config.properties"));
		PropertyConfigurator.configure("./config/log4jPublisher.properties");
		logger.info("the configuration loaded is ");
		logger.info(config.toString());		
	}
	
	public static String getParameter(String name){
		return config.getProperty(name).trim();
	}
	

}
