package com.atos.ari;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.atos.ari.event.Event;
import com.atos.ari.event.Message;
import com.atos.ari.mqtt.MqttHelper;
import com.atos.ari.uncap.UncapParser;
import com.atos.ari.uncap.configuration.Configuration;


public class StartUncapAdaptor {
	
	private static MqttHelper mqttHelperExternal;
	private static MqttHelper mqttHelperCep;
	private static UDPClientHelper udpClientHelper;
	private static UDPServerHelper udpServerHelper;
	public static Properties config;	
	final static Logger logger = Logger.getLogger(StartUncapAdaptor.class);	
	


	public static void main(String[] args) throws Throwable{
		try{
			loadConfiguration();
			UncapParser parser = new UncapParser();
			String sdev = config.getProperty("uncap.devices");
			logger.debug("Devices allowed: " + sdev);
			String[] devices = sdev.split(",");
			mqttHelperExternal.connect();
			//mqttHelperCep.connect();
			while (true){
				try{
					processMessage(devices, parser);
					Thread.sleep(1000);
				}catch (Throwable e){
					logger.error("Error processing message " + e);
					logger.debug("Restarting message processing " + e);
					if (mqttHelperExternal.isConnected()){
						mqttHelperExternal.disconnect();					
					}
					mqttHelperExternal.connect();
					/*
					if (!mqttHelperCep.isConnected()){
						mqttHelperCep.connect();						
					}*/
				}
			}
		}catch (Throwable e){
			logger.error(e);
			System.exit(-1);
		}
	}
	
	private static void processMessage(String[] devices, UncapParser parser)  throws Throwable{
		String smessage = mqttHelperExternal.get();
		if (smessage!=null){
			Message  objMessage = UncapParser.format(smessage, devices);
			int i = 0;
			List<Event> cepInputEvents = objMessage.getCepEvents();
			while (i < objMessage.getCepEvents().size()){
				Event eventItem = cepInputEvents.get(i++);
				String cepInputMessage = eventItem.toString();
				udpClientHelper.sendEventToCEP_UDP(cepInputMessage);
				//mqttHelperCep.publish(cepInputMessage);
				logger.info("message sent to CEP " + cepInputMessage);
			}
			String mesReceived = udpServerHelper.receiveEventFromCEP_UDP(1000);
			//String mesReceived = mqttHelperCep.get();
			if (mesReceived != null){
				Event event = new Event(mesReceived);
				String complexEventName = event.getName();
				String cepOutput = parser.formatComplexEvent(complexEventName, objMessage);
				//String topicByDoctor = buildTopicByDoctor(objMessage);
				//String topicByPatient = buildTopicByPatient(objMessage);				
				//mqttHelperExternal.publish(cepOutput, topicByDoctor);
				//mqttHelperExternal.publish(cepOutput, topicByPatient);
				sendHTTPPut(cepOutput, objMessage);
			}else{
				logger.info("not returned message from CEP for input Message: ");
			}
			
		}
	} 

	private static String buildTopicByDoctor(Message objMessage) {
		StringBuilder sb = new StringBuilder();
		sb.append(objMessage.getAttsMesagge().get(UncapParser.DOCTOR_ID_KEY));
		sb.append("/patients/");
		sb.append(objMessage.getAttsMesagge().get(UncapParser.USER_ID_KEY));
		sb.append("/alarm");		
		return sb.toString();
	}
	
	private static String buildTopicByPatient(Message objMessage) {
		StringBuilder sb = new StringBuilder();
		sb.append(objMessage.getAttsMesagge().get(UncapParser.USER_ID_KEY));
		sb.append("/alarm");		
		return sb.toString();
	}
	

	private static void loadConfiguration() throws Exception {
		config = Configuration.getInstance().config;
		config.load(new FileInputStream("./config/config.properties"));
		PropertyConfigurator.configure("./config/log4jUncap.properties");
		logger.info("the configuration loaded is ");
		logger.info(config.toString());
		mqttHelperExternal = new MqttHelper(config.getProperty("uncap.inputTopic"),
									config.getProperty("uncap.outputTopic"),
									Integer.parseInt(config.getProperty("uncap.qos")),
									config.getProperty("uncap.urlbroker"),
									config.getProperty("uncap.clientId"),
									config.getProperty("uncap.user"),
									config.getProperty("uncap.password"));

		udpClientHelper = new UDPClientHelper(config.getProperty("uncap.cep.udp.host"),
																		config.getProperty("uncap.cep.udp.inputport"));
		udpServerHelper = new UDPServerHelper(config.getProperty("uncap.cep.udp.host"), 
																		config.getProperty("uncap.cep.udp.outputport"));

		//The Uncap adaptor publish in tescepInput, CEP process and publish in testCepOutput. Uncap adaptor read from cep output
		//the perspective is different to the other mqtt helper
		/*
		mqttHelperCep = 
				new MqttHelper("testcepOutput", "testcepInput", 
											0, 
											"tcp://localhost:2883", "", null, null);
											*/

	}
	
	public static void sendHTTPPut(String message, Message objMessage){
		
		try{
			logger.debug("trying to send via http " + message);  
			URL url = new URL(config.getProperty("uncap.raptor.url"));
			HttpsURLConnection httpCon = (HttpsURLConnection) url.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("PUT");
			String header = objMessage.getAttsMesagge().get("header");
			httpCon.setRequestProperty("Authorization", header);
			httpCon.setRequestProperty("Content-Type", "application/json");
			OutputStreamWriter out = new OutputStreamWriter(
			    httpCon.getOutputStream());
			out.write(message);
			out.close();
			logger.debug("Response message: " + httpCon.getResponseMessage());
		}catch (Exception e){
			logger.error("Error sending http request" + e);
		}
		
	}
	

}
