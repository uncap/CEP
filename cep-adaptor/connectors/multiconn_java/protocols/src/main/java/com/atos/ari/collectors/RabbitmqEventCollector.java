package com.atos.ari.collectors;

import java.util.Queue;

import org.apache.log4j.Logger;

import com.atos.ari.EventCollector;
import com.atos.ari.StartCollector;
import com.atos.ari.event.Event;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitmqEventCollector extends EventCollector{
	
	private QueueingConsumer consumer;
	final static Logger logger = Logger.getLogger(RabbitmqEventCollector.class);	
	
	public RabbitmqEventCollector(Queue<Event> queue) {
		super(queue);
		try{
	        ConnectionFactory factory = new ConnectionFactory();
	        factory.setHost(StartCollector.getParameter("rabbit.host"));
	        factory.setPort(Integer.valueOf(StartCollector.getParameter("rabbit.port")));
	        Connection connection = factory.newConnection();
	        Channel channel = connection.createChannel();
	        String exchangeName = StartCollector.getParameter("rabbit.exchangeToSubscribe");
	        //if (!rmq_existsExchange(channel, exchangeName)){
	        	channel.exchangeDeclare(exchangeName, "topic");
	        //}
	        String queueName = channel.queueDeclare(StartCollector.getParameter("rabbit.queue.subscribe.name"), true, true, false, null).getQueue();        
	        channel.queueBind(queueName, exchangeName, "#");
	        consumer = new QueueingConsumer(channel);
	        channel.basicConsume(queueName, true, consumer);
	        
			
		}catch (Exception e){
			System.out.println("Configuration error " + e);
		}
	}

	@Override
	public void collectData() throws Exception {
        QueueingConsumer.Delivery delivery = consumer.nextDelivery();
        String message = new String(delivery.getBody());
        String routingKey = delivery.getEnvelope().getRoutingKey();
        logger.info(" [x] Received '" + routingKey + "':'" + message + "'");
        queue.add(new Event(message));
        
	}
	
	
	
	
	

}
