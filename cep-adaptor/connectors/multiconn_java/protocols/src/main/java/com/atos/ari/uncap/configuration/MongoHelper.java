package com.atos.ari.uncap.configuration;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.bson.Document;

import com.atos.ari.event.Attribute;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoHelper {
	final static Logger logger = Logger.getLogger(MongoHelper.class);	
	private static  MongoHelper instance=null;
	private String host;
	private int port;
	private String db;
	private MongoDatabase mongoDB;
	private MongoHelper()  throws Exception{
		super();
    	this.host =Configuration.getInstance().config.getProperty("mongo.host");
    	this.port = Integer.parseInt(Configuration.getInstance().config.getProperty("mongo.port"));
    	this.db = Configuration.getInstance().config.getProperty("mongo.dbname");
		MongoClient mongoClient = new MongoClient( this.host, this.port );
		mongoDB = mongoClient.getDatabase(db);
	}
	
	public static  MongoHelper  getInstance() throws Exception{
		if (instance == null){
			instance = new MongoHelper();
		}
		return instance;
	}
	
	public List<Attribute> getConfigurationFields(String user_id, String device_id){
		List<Attribute> res = new ArrayList<Attribute>();
		Document docRules = null;
		MongoCollection<Document> coll  =mongoDB.getCollection("configurations");
		Document docQuery = new Document("user_id",user_id ).append("device_id", device_id);
		Document docDefaultQuery = new Document("user_id","default" ).append("device_id", device_id);		
		FindIterable<Document> iterable = coll.find(docQuery);
		docRules = iterable.first();
		if (docRules==null){
			logger.debug("No configuration for user_id " + user_id + " default configuration is returned");
			iterable = coll.find(docDefaultQuery);
			docRules = iterable.first();
		}
		
		List<Document> rules = (List<Document>)  docRules.get("rules");
		int i = 0;
		while (i<rules.size()){
			Document docitem =  rules.get(i);
			String key = docitem.entrySet().iterator().next().getKey();
			List<Document> docFields = (List<Document>) docitem.get(key);
			int j = 0;
			while (j<docFields.size()){
				Document docFieldItem = docFields.get(j);
				String name = docFieldItem.getString("field_name");
				String value = docFieldItem.getString("field_value");
				res.add(new Attribute("int", name, value));
				j++;
			}
			i++;
		}
		
		
		
		return res;
	}
	

}
