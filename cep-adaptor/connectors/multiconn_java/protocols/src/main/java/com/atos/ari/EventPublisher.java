package com.atos.ari;

import java.util.Queue;


public abstract class EventPublisher {
	
	protected Queue<String> queue;
	
	public EventPublisher(Queue<String> queue){
		this.queue = queue;
	}
 
	public void publishData() throws Exception{
		System.out.println("Error: You must implement publishData method ");
	};

}
