package test;

import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.util.Properties;

import junit.framework.TestCase;

import com.atos.ari.Utils;
import com.atos.ari.uncap.UncapParser;

public class UncapParserTest  extends TestCase {
	
	public static Properties config;
	public static String[] devices;

	public UncapParserTest(String name)  throws Exception{
		super(name);		
		config = new Properties();
		config.load(new FileInputStream("./config/config.properties"));
		String sdev = config.getProperty("uncap.devices");
		devices = sdev.split(",");
	}
	
	public void testUncapParser(){
		try{
	    	String message=Utils.readFile("./src/test/resources/glucometer.json",Charset.forName("UTF-8"));
	    	UncapParser.format(message,devices);
	    	assertTrue(true);
		}catch (Exception e){
			System.out.println(e);			
		}
	}
	
	
	
	

}
