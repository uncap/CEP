package test;

import java.util.List;
import java.util.Properties;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.atos.ari.event.Attribute;
import com.atos.ari.uncap.configuration.Configuration;
import com.atos.ari.uncap.configuration.MongoHelper;

public class UncapMongoTest extends TestCase{
	
	final static Logger logger = Logger.getLogger(UncapMongoTest.class);	
	Properties config = null;

	public UncapMongoTest(String name) throws Exception{
		super(name);
		config = Configuration.getInstance().config;
		PropertyConfigurator.configure("./config/log4jUncap.properties");
	}
	
	public void testGetFieldsConfiguration() throws Exception{
		MongoHelper mh = MongoHelper.getInstance(); 
		List<Attribute> res = mh.getConfigurationFields("Miguel Rodríguez","blood_glucose");
		logger.debug(res);
	}
	
	
	

}
