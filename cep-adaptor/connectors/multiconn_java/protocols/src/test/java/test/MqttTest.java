package test;

import java.nio.charset.Charset;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.log4j.PropertyConfigurator;

import com.atos.ari.Utils;
import com.atos.ari.mqtt.MqttHelper;


/**
 * Unit test for simple App.
 */
public class MqttTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MqttTest( String testName )
    {
        super( testName );
		PropertyConfigurator.configure("./src/test/resources/log4j.properties");        
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MqttTest.class );
    }

    /*
     * Rigourous Test :-)
     */
    public void testMqttHelper()
    {
    	try{
        	MqttHelper mqttHelper = 
        			new MqttHelper(	"testInputTopic",
        							"testInputTopic",
        							1,
        							"tcp://localhost:1883",
        							"testClientid",
        							null,null);
        	mqttHelper.connect();
        	String message=Utils.readFile("./src/test/resources/glucometer.json",
        			Charset.forName("UTF-8"));
        	mqttHelper.publish(message);
        	Thread.sleep(1000);
        	message = mqttHelper.get();
        	mqttHelper.disconnect();
        	assertTrue(message !=  null);
    	}catch (Exception e){
    		System.out.println(e);
    	}

    }
    
}
