package test;

import java.nio.charset.Charset;

import junit.framework.TestCase;

import org.apache.log4j.PropertyConfigurator;

import com.atos.ari.Utils;
import com.atos.ari.mqtt.MqttHelper;

public class MqttTestRaptor extends TestCase{
	
	
	public MqttTestRaptor(String name) {
		super(name);
		PropertyConfigurator.configure("./src/test/resources/log4j.properties");		
	}

	public void testRaptor(){
		String toPublish="14476753261027da5a210c0ea46e28d05aee90889f51f/from";
		String toRead="25217f35183c5cdffb28f0674cd0e538524da754/1444834821232d7e513234caa4d98a74284c40ce62a93/streams/alarms/updates";
    	try{
        	MqttHelper mqttHelper = 
        			new MqttHelper(toRead,
        							toPublish,
        							2,
        							"tcp://api.raptorbox.eu:1883",
        							"testClientid",
        							"compose","shines");
        	mqttHelper.connect();
        	String message=Utils.readFile("./src/test/resources/alarm.json",
        			Charset.forName("UTF-8"));
        	mqttHelper.publish(message);
        	Thread.sleep(20000);
        	message = mqttHelper.get();
        	System.out.println("message receivied "  + message);
        	mqttHelper.disconnect();
        	assertTrue(message !=  null);
    	}catch (Exception e){
    		
    	}
	}

}
